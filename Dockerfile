FROM node:alpine

RUN mkdir -p /code
WORKDIR /code
COPY . /code

RUN yarn install

EXPOSE 3000

ENTRYPOINT ["yarn"]
CMD ["start"]
