import React from 'react';
import {ApolloProvider} from 'react-apollo';
import {BrowserRouter} from 'react-router-dom';

import client from './util/Client';

import Routes from './routes';

const App = () => (
  <BrowserRouter>
    <ApolloProvider client={client}>
      <Routes />
    </ApolloProvider>
  </BrowserRouter>
);

export default App;
