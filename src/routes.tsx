import React from 'react';

import {Route, Switch} from 'react-router-dom';

import AboutMe from './pages/AboutMe';
import Art from './pages/Art';
import Contact from './pages/Contact';
import Home from './pages/Home';

const Routes = () => (
  <Switch>
    <Route exact={true} path="/" component={Home} />
    <Route path="/aboutme" component={AboutMe} />
    <Route path="/art" component={Art} />
    <Route path="/contact" component={Contact} />
  </Switch>
);

export default Routes;
