const randomImage = "images/abstract/abstract_five/abstract_5.jpg";
const mocks = {
    String: () => "Does this work?",
    ImageNode: () => ({
        title: () => "Random ass name",
        sourceStandard: () => randomImage,
        sourceSmall: () => randomImage,
        sourceLarge: () => randomImage
    })
};

export default mocks;
