import { InMemoryCache } from "apollo-cache-inmemory";
import ApolloClient from "apollo-client";
import { ApolloLink } from "apollo-link";
import { onError } from "apollo-link-error";
import { HttpLink } from "apollo-link-http";
import { SchemaLink } from "apollo-link-schema";

// For mocking
import { buildClientSchema } from "graphql";
import { addMockFunctionsToSchema } from "graphql-tools";
import schemaFile from "./schema.json";
import mocks from "./mocks";

const schema = buildClientSchema(schemaFile.data);
addMockFunctionsToSchema({ schema, mocks });

const uri = "/graphql/";

let link;
if (process.env.NODE_ENV === "development") {
    link = new SchemaLink({ schema });
} else {
    link = ApolloLink.from([
        onError(({ graphQLErrors, networkError }) => {
            if (graphQLErrors) {
                graphQLErrors.map(({ message, locations, path }) =>
                    console.log(
                        `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
                    )
                );
            }
            if (networkError) {
                console.log(`[Network error]: ${networkError}`);
            }
        }),
        new HttpLink({
            uri,
            credentials: "same-origin",
            fetch
        })
    ]);
}
export default new ApolloClient({
    link,
    cache: new InMemoryCache()
});
