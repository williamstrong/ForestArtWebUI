/** @jsx jsx */
import { jsx } from "@emotion/core";

export interface HeaderProps {
    css: any;
}

function Header(props: HeaderProps): JSX.Element {
    return <div css={props.css} />;
}

export default Header;
