import styled from "@emotion/styled";

const HorizontalRule = styled.hr({
    width: "60%",
    margin: "auto",
    borderTop: "2px solid #b6b6b6"
});

export default HorizontalRule;
