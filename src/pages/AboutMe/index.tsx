import React from 'react';

import Footer from '../shared/Footer';
import ProfileContainer from './containers/ProfileContainer';
import AboutMeNav from './nav/AboutMeNav';

function AboutMe(): JSX.Element {
  return (
    <div>
      <AboutMeNav />
      <ProfileContainer />
      <Footer />
    </div>
  );
}

export default AboutMe;
