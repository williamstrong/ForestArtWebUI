/** @jsx jsx */
import { jsx } from "@emotion/core";

import NavBar from "../../shared/NavBar";

import { name, navPosition, pages } from "../constants/Navigation";

const AboutMeNav = () => <NavBar css={navPosition} pages={pages} name={name} />;

export default AboutMeNav;
