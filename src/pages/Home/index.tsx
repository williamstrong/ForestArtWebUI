import React from "react";

import Footer from "../shared/Footer";
import HorizontalRule from "../shared/HorizontalRule";
import NavBar from "../shared/NavBar";
import PreviewContainer from "../shared/preview/container/PreviewContainer";

const pages = [
    {
        name: "About Me",
        href: "/aboutme"
    },
    {
        name: "Art",
        href: "/art"
    },
    {
        name: "Contact",
        href: "/contact"
    }
];

function Home() {
    return (
        <>
            <NavBar name="Alexandra Forest" pages={pages} />
            <HorizontalRule />
            <PreviewContainer />
            <HorizontalRule />
            <Footer />
        </>
    );
}

export default Home;
